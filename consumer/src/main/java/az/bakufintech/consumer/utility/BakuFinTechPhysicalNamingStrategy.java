package az.bakufintech.consumer.utility;

import org.atteo.evo.inflector.English;
import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;
import org.springframework.boot.orm.jpa.hibernate.SpringPhysicalNamingStrategy;

import java.util.Locale;

public class BakuFinTechPhysicalNamingStrategy extends SpringPhysicalNamingStrategy {


    @Override
    public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment jdbcEnvironment) {
        if (classExistsInLibraryPackage(name.getText())) {
            return new Identifier(addUnderscores(addLib(pluraliseWord(name.getText()))), name.isQuoted());
        }
        return new Identifier(addUnderscores(addAg(pluraliseWord(name.getText()))), name.isQuoted());
    }

    public String addUnderscores(String name) {
        StringBuilder buf = new StringBuilder(name.replace('.', '_'));

        for (int i = 1; i < buf.length() - 1; ++i) {
            if (Character.isLowerCase(buf.charAt(i - 1)) && Character.isUpperCase(buf.charAt(i)) && Character.isLowerCase(buf.charAt(i + 1))) {
                buf.insert(i++, '_');
            }
        }
        return buf.toString().toLowerCase(Locale.ROOT);
    }

    public String pluraliseWord(String word) {
        return English.plural(word);
    }

    public boolean classExistsInLibraryPackage(String className) {
        className = "az.bakufintech.salesrecordonsumer.model.library." + className;
        try {
            Class.forName(className);
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }


    public String addLib(String name) {
        return "LIB_" + name;
    }

    public String addAg(String name) {
        return "BFT_" + name;
    }
}