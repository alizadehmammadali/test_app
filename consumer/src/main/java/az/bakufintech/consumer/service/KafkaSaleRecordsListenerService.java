package az.bakufintech.consumer.service;

import az.bakufintech.consumer.dao.SaleDao;
import az.bakufintech.consumer.model.Sale;
import az.bakufintech.consumer.model.dto.RecordsCountDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaSaleRecordsListenerService {

    private final SaleDao saleDao;
    private final SimpMessagingTemplate template;

    @KafkaListener(
            topics = "${bakufintech.kafka.topic}",
            groupId = "${bakufintech.kafka.group.id}"
    )
    public void listen(List<Sale> sales) {
        saleDao.saveAll(sales);
        long recordsCount = saleDao.count();
        RecordsCountDto recordsCountDto=new RecordsCountDto();
        recordsCountDto.setCount(recordsCount);
        template.convertAndSend("/topic/recordsCounter",recordsCountDto);
    }


}
