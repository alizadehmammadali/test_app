package az.bakufintech.consumer.model.base;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@MappedSuperclass
@Data
public class BaseEntity implements Serializable {
    private static final long serialVersionUID = 596645378341217299L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;

    @JsonFormat(pattern = "dd.MM.yyyy")
    @Column(name = "REG_DATE", nullable = false, updatable = false)
    @CreatedDate
    private LocalDate regDate;

    @Column(name = "EDIT_DATE")
    @LastModifiedDate
    private LocalDate editDate;

}
