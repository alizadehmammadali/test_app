package az.bakufintech.consumer.model.dto;

import lombok.Data;

@Data
public class RecordsCountDto {
    private Long count;

    @Override
    public String toString() {
        return "{" +
                "count=" + count +
                '}';
    }
}
