let stompClient = null;
let socket = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
}

function connect(sockJSClient, messageBrokerUrl) {
    socket = new SockJS(sockJSClient);
    stompClient = Stomp.over(socket);
    stompClient.reconnect_delay = 5000;
    stompClient.connect({}, function (frame) {
            setConnected(true);
            stompClient.subscribe(messageBrokerUrl, function (data) {
                $("#recordsCount").text(JSON.parse(data.body).count);
            });
        },
        function (error) {
            //Reconnect
            setTimeout(function () {
                connect(sockJSClient, messageBrokerUrl);
            }, 3000);
        }
    );
}


function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}


function process(url) {
    $.ajax({
        url: url,
        type: 'get',
        data: "",
        success: function (data, textStatus, jQxhr) {
            console.log(data);
        },
        error: function (jqXhr, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
}



