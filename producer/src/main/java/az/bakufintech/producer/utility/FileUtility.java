package az.bakufintech.producer.utility;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class FileUtility {

    public static void setValue(String filePath, String key, String value) {
        try {
            String newPair = key + ":" + value;
            String oldValue = getValue(filePath, key);
            if (oldValue != null) {
                try (Stream<String> lines = Files.lines(Paths.get(filePath))) {
                    String oldPair = key + ":" + oldValue;
                    List<String> replaced = lines.map(s -> s.replace(oldPair.trim(), newPair.trim())).collect(Collectors.toList());
                    Files.write(Paths.get(filePath), replaced, StandardCharsets.UTF_8);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                Files.write(Paths.get(filePath), Collections.singletonList(newPair), StandardCharsets.UTF_8);
            }
        } catch (IOException e) {
            log.error("Exception occurred in writing file " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static String getValue(String filePath, String key) {
        String value = null;
        try (Stream<String> lines = Files.lines(Paths.get(filePath))) {
            value = lines.filter(line -> line.contains(key + ":")).map(s -> s.split(":")[1]).findFirst().orElse(null);
        } catch (IOException e) {
            log.error("Exception occurred in reading file " + e.getMessage());
            e.printStackTrace();
        }
        return value;
    }


}
