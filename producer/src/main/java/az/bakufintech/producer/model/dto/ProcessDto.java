package az.bakufintech.producer.model.dto;

import lombok.Data;

@Data
public class ProcessDto {
    private String message;
}
