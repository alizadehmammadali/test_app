package az.bakufintech.producer.controller;

import az.bakufintech.producer.model.dto.ProcessDto;
import az.bakufintech.producer.service.CsvFileReaderService;
import az.bakufintech.producer.service.thread.CsvFileReaderThread;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pro")
@Slf4j
public class ResourceController {
    public CsvFileReaderThread csvFileReaderThread;

    public ResourceController(CsvFileReaderService cvsFileReaderService,
                              @Value("${bakufintech.kafka.records-write-sleep-interval}") Integer sleepInterval,
                              @Value("${bakufintech.csv.read.index}") String readIndex,
                              @Value("${bakufintech.values.filePath}") String valuesFilePath
    ) {
        this.csvFileReaderThread = new CsvFileReaderThread(cvsFileReaderService,sleepInterval,valuesFilePath,readIndex);
        this.csvFileReaderThread.start();
    }

    @GetMapping("/start")
    public ProcessDto startProcess() {
        csvFileReaderThread.startProcess();
        ProcessDto processDto = new ProcessDto();
        processDto.setMessage("Process has been successfully started");
        return processDto;
    }

    @GetMapping("/stop")
    public ProcessDto stopProcess() {
        csvFileReaderThread.stopProcess();
        ProcessDto processDto = new ProcessDto();
        processDto.setMessage("Process has been successfully stopped");
        return processDto;
    }
}
