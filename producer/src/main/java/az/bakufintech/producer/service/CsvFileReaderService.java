package az.bakufintech.producer.service;


import az.bakufintech.producer.model.Sale;

import java.io.InputStream;
import java.util.List;

public interface CsvFileReaderService {
     List<Sale> getSaleRecordsFromCSV(Integer readStartIndex, InputStream in);
     int sendSaleRecordsToKafka(int readStartIndex);
}
