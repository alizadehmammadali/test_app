package az.bakufintech.producer.service.impl;

import az.bakufintech.producer.model.Sale;
import az.bakufintech.producer.model.enums.SalesChannel;
import az.bakufintech.producer.service.CsvFileReaderService;
import az.bakufintech.producer.utility.FileUtility;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class CsvFileReaderServiceImpl implements CsvFileReaderService {

    @Value("${bakufintech.csv.line.delimiter}")
    private String lineDelimiter;

    @Value("${bakufintech.csv.read.count}")
    private Integer readCount;

    @Value("${bakufintech.csv.read.index}")
    private String readIndex;

    @Value("${bakufintech.values.filePath}")
    private String filePath;

    @Value("${bakufintech.kafka.topic}")
    private String kafkaTopic;

    private final KafkaTemplate<String, Sale> kafkaTemplate;

    @Override
    public List<Sale> getSaleRecordsFromCSV(Integer readStartIndex, InputStream in) {
        List<Sale> saleList = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            saleList = br.lines().skip(readStartIndex).limit(readCount).map(mapToItem).collect(Collectors.toList());
            br.close();
            //Reaching the end of the file
            if (saleList.size() == 0) {
                FileUtility.setValue(Objects.requireNonNull(filePath), readIndex, "-1");
            }
        } catch (IOException e) {
            e.printStackTrace();
            log.error("Exception occurred in file reader: " + e.getMessage());
        }
        return saleList;
    }

    private Function<String, Sale> mapToItem = (line) -> {
        String[] lineRecord = line.split(lineDelimiter);
        Sale sale = new Sale();
        sale.setRegion(lineRecord[0]);
        sale.setCountry(lineRecord[1]);
        sale.setItemType(lineRecord[2]);
        sale.setSalesChannel(SalesChannel.valueOf(lineRecord[3]));
        sale.setOrderPriority(lineRecord[4]);
        sale.setOrderDate(mapStringToLocalDate.apply(lineRecord[5]));
        sale.setOrderId(Integer.valueOf(lineRecord[6]));
        sale.setShipDate(mapStringToLocalDate.apply(lineRecord[7]));
        sale.setUnitsSold(Integer.valueOf(lineRecord[8]));
        sale.setUnitPrice(Double.valueOf(lineRecord[9]));
        sale.setUnitCost(Double.valueOf(lineRecord[10]));
        sale.setTotalRevenue(Double.valueOf(lineRecord[11]));
        sale.setTotalCost(Double.valueOf(lineRecord[12]));
        sale.setTotalProfit(Double.valueOf(lineRecord[13]));
        return sale;
    };

    private static Function<String, LocalDate> mapStringToLocalDate = date ->
    {
        final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("M/d/yyyy");
        return LocalDate.parse(date, dtf);
    };

    @Override
    public int sendSaleRecordsToKafka(int readStartIndex) {
        AtomicInteger writeIndexCounter = new AtomicInteger(0);
        List<Sale> sales = getSaleRecordsFromCSV(readStartIndex, getClass().getResourceAsStream("/csv/sale_records.csv"));
        for (Sale sale : sales) {
            kafkaTemplate.send(kafkaTopic, UUID.randomUUID().toString(), sale);
            writeIndexCounter.getAndIncrement();
            log.info("Sent To Kafka Record: " + sale + " Index " + writeIndexCounter.get());
        }
        int next = readStartIndex + writeIndexCounter.get();
        FileUtility.setValue(Objects.requireNonNull(filePath), readIndex, String.valueOf(next));
        log.info("Start Index- " + readStartIndex + " " + " WriteIndexCount " + writeIndexCounter.get() + " Next " + next);
        return readStartIndex + writeIndexCounter.get();
    }


}
