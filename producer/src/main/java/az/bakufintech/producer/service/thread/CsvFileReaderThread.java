package az.bakufintech.producer.service.thread;


import az.bakufintech.producer.service.CsvFileReaderService;
import az.bakufintech.producer.utility.FileUtility;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

@Slf4j
public class CsvFileReaderThread extends Thread {
    private volatile boolean running = false;
    private final Integer sleepInterval;
    private final String readIndex;
    private final String valuesFilePath;
    private final CsvFileReaderService cvsFileReaderService;

    public CsvFileReaderThread(CsvFileReaderService cvsFileReaderService,
                               Integer sleepInterval,
                               String valuesFilePath,
                               String readIndex) {
        this.cvsFileReaderService = cvsFileReaderService;
        this.sleepInterval = sleepInterval;
        this.valuesFilePath = valuesFilePath;
        this.readIndex = readIndex;
    }

    public void startProcess() {
        running = true;
    }

    public void stopProcess() {
        running = false;
    }

    public void run() {
        try {
            while (true) {
                if (running) {  //Flag to start and stop process
                    int currentRecordIndex = getCurrentRecordIndex();
                    if (currentRecordIndex == -1) { //reached end of the file
                        break;
                    }
                    int readRecordIndex = cvsFileReaderService.sendSaleRecordsToKafka(currentRecordIndex);
                    setRecordIndex(readRecordIndex);
                    Thread.sleep(sleepInterval);
                }
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            log.error("Thread was interrupted, Failed to complete operation");
        }
    }

    private Integer getCurrentRecordIndex() {
        String currentIndex = FileUtility.getValue(Objects.requireNonNull(valuesFilePath), readIndex);
        System.out.println("CurrentIndex "+currentIndex);
        if (currentIndex == null) {
            return 1; //First Line is header
        }
        return Integer.valueOf(currentIndex);
    }

    private void setRecordIndex(Integer currentIndex) {
        FileUtility.setValue(Objects.requireNonNull(valuesFilePath), readIndex, String.valueOf(currentIndex));
    }


}
