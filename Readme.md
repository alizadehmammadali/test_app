# Test-app

### Todos

### Docker installation

After the installing Docker to the server you must add 4 system environment variables to the server to run the application. 
https://linuxize.com/post/how-to-set-and-list-environment-variables-in-linux/

```sh
$ export HOST_IP=$(ip route get 8.8.8.8 | head -1 | awk '{print $7}')
$ export DB_USER=yourdbuser
$ export DB_NAME=yourdbname
$ export DB_PASSWORD=yourdbpassword
$ docker-compose up -d
```

